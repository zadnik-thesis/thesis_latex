%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Untimed Reference Model}
\label{untimed}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

A Python program \pythoninline{refftta} \citep{refftta} was developed as a high level model.
The purpose of this model is to get familiar with the idea of the algorithm and architecture and to generate reference results which will be used later in a timed simulation and final RTL implementation.
Section \ref{untimed:overview} gives an overview of the program and its features.
Section \ref{untimed:detailed} is dedicated to the description of the FFT algorithm used by \pythoninline{refftta}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Program Overview}
\label{untimed:overview}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

It follows the same dataflow as in the \zk{TTA} processor.
\zk{FU}s are modelled as functions and their results correspond to the significant computation steps in the algorithm.
However, as an untimed model it does not provide any details about an efficiency of the program/architecture and makes some assumptions and shortcuts not present in the lower levels of abstraction.

The program is written in a way which makes it possible and to retrieve data (intermediate results, input addresses, etc.) from the algorithm.
The retrieved data can be used, e.g. for the evaluation in the terminal window or exported into a file as reference data.
All computations are done using a 64-bit floating point data format.
For the purpose of generating the reference files the data is converted to a fixed point representation.

A Python package NumPy \citep{numpy} was used for the numerical computing.
It is a library optimized for computations with array objects in a same fashion as MATLAB.
Apart from the array objects it can be used as a general numerical computing library since it contains many functions useful for numerical computing.
While NumPy provides the necessary functionality and speed comparable to MATLAB, Python as a general purpose programming language is well suited to conveniently handle the rest of the features (such as command line options and file generation).

% It supports \zk{FFT} sizes ($N$) from 8 ($2^3$) to 16384 ($2^{14}$) with $N$ being a power of two.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Detailed Description}
\label{untimed:detailed}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The code in Figure \ref{lst:main_py} is the core part of the Python program.
It has been modified and shortened for a better readability for the purpose of this document.
Therefore, it should be treated more as a pseudo-code despite using a Python syntax.

A computation of one butterfly consists of several fundamental steps:

\begin{enumerate}
	\item Generate input addresses for a butterfly and fetch the butterfly inputs.
	\item Generate \emph{k}-coefficients for twiddle factors.
	\item Based on the \emph{k}, fetch a twiddle factors from an \zk{LUT}.
	\item Multiply the butterfly inputs with twiddle factors.
	\item Perform the butterfly operation (complex adder).
	\item Store the results back to the same addresses.
\end{enumerate}

\begin{figure}
\begin{python}
# Generate lookup table of twiddle factors
tf_lut = gen_tf_lookup(2**14)

# Compute FFT (input sequence x)
for stage in range(total_stages(Nexp)):
    # Whether to use the radix-2 stage (radix-4 is default)
    rdx2_flag = rdx2_stage(stage, Nexp);
    # For every butterfly (4 samples)
    for lin_idx in range(0, Nexp, 4):
        addr = ag(lin_idx, stage, Nexp)
        k    = gen_tf_k(lin_idx, stage, Nexp, rdx2_flag)
        tf   = tfg(k_sc, tf_lut, Nexp)
        # Butterfly input
        for i in range(4):
            X[i] = x[addr[i]]
        P    = multiply(tf, X)
        # Compute butterfly
        Y    = cadd(P, rdx2_flag)
        # Assign the result to the original memory
        for i in range(4):
            x[addr[i]] = Y[i]

# Rearrange result
for i in range(0, Nexp):
    idx = bit_reverse(i, Nexp)
    y[i] = x[idx]
\end{python}
	\caption{Abbreviated Python script for computing \zk{FFT}}
\label{lst:main_py}
\end{figure}

First the program generates a lookup table of twiddle factors of size 2049 ($N/8+1$ for $N = 2^{14}$).
The information about the current FFT size is stored within the $Nexp$ parameter: $N = 2^{Nexp}$.
Then it iterates over the computation stages, each time checking which radix to use.
In every stage, linear index (\pythoninline{lin_idx}) is iterated with a step of four.
The algorithm computes one radix-4 butterfly or two radix-2 butterflies at the time.
In the final implementation, the linear index iterates with a step of one and the results are completed sequentially.

Based on the linear counter, four addresses of input operands are generated (\pythoninline{ag}).
The addresses are generated by taking two (one in case of $Nexp$ being odd) \zk{LSB}s (\zkratkatext{LSB}s) and placing them between the rest of the bits.
The exact position where to insert the \zk{LSB}s is determined by \pythoninline{stage} (see Section \ref{timed:ag}).
In the last stage, the operands are accessed linearly.

Next the indices ($k$) for twiddle factors ($W_N^k$) are computed by \pythoninline{gen_tf_k}.
The indices computed are different for radix-4 and radix-2, therefore we need to specify \pythoninline{rdx2_flag} in the input.
From Figure \ref{fig:twiddles} it is apparent that twiddle factors for smaller \zk{FFT}s can be derived from the larger sizes by taking only every 2nd, 4th, 8th, etc. twiddle factor.
Therefore, after the generation of $k$-s the function \pythoninline{gen_tf_k} also performs scaling according to the current \zk{FFT} size.
The scaling itself is just bit-shifting $k$-s to the left as necessary.
By using the $k$-scaling we do not need to pre-compute a twiddle factor \zk{LUT} for each \zk{FFT} size separately since it is enough to have only one for the largest supported $N$.

After getting the necessary $k$-s we can feed them to the twiddle factor generator and get four complex valued twiddle factors.
The twiddle factor generator generates the appropriate values using principles described in Section \ref{fft:twiddles}.

Now is the time to compute the butterfly itself.
The butterfly input (\pythoninline{X}) are four values selected from the input sequence (\pythoninline{x}) specified by the generated addresses (\pythoninline{addr}).
We multiply the butterfly input with twiddle factors and feed the products (\pythoninline{P}) into a modified complex adder (\pythoninline{cadd}).
This function either performs one radix-4 butterfly according to \ref{eq:rdx4_but} or two radix-2 butterflies according to \ref{eq:rdx2_but} based on \pythoninline{rdx2_flag}.

The result of the butterfly is stored into \pythoninline{Y} and these values are stored back to \pythoninline{x} to the same addresses from where they were read to \pythoninline{X}.
This technique is called \textit{in-place computation}.
It is used to save memory by using only one vector \pythoninline{x} where both inputs and outputs are stored.
When the butterfly computation is completed, its results rewrite the input values.

After performing all butterfly computations in a given stage, the algorithm proceeds to the next stage, checks whether this stage is radix-2 and radix-4 and repeats the above procedure.
After running through the whole computation the result is in a permuted order and needs to be reordered.
This is done by reversing bit pairs (in case of $Nexp$ being odd the \zk{LSB} pair is not a bit pair but a single bit) of the indices of the result's samples.
