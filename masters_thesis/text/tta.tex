%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Transport Triggered Architecture}
\label{tta}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The proposed architecture for our FFT application is based on a \zkratkatext{TTA} (\zk{TTA}) template \citep{corporal98}.
The architecture resembles a \zk{VLIW} (\zkratkatext{VLIW}) processor architecture (\citep{fisher83}, \citep{philips11}).
Its principle and advantages over \zk{VLIW} are described in Section \ref{tta:overview}.
A special tool for designing \zk{TTA} processors, developed by Department of Pervasive Computing at \zk{TUT}, is introduced in Section \ref{tta:tce}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{TTA Overview}
\label{tta:overview}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Transport Triggered Architecture is a processor architecture where a designer has a full control over the data transports, specifying both a source and a destination.
The instruction set has only one instruction: \textit{move}.
To perform an operation (e.g. AND) we first \textit{move} an operand data to an operand input of a \zk{FU} (\zkratkatext{FU}).
Operation itself is triggered by \textit{moving} a data to a trigger input of the \zk{FU}.
After the operation is performed we can collect the result from the output of the \zk{FU}.

The data is transported using an \textit{interconnection network} which consists of a number of parallel buses connected to the inputs and outputs of \zk{FU}s.
The maximum number of \textit{moves} performed in one clock cycle is limited by the number of buses.
One bus can perform only one data transport per clock cycle (since it is just a wire).
The number of buses is customizable as well as which \zk{FU} inputs/outputs are connected to which buses.
Furthermore, there is no limitation on the number of inputs and outputs of \zk{FU}s.

The key property of \zk{TTA} is that we can \textit{move} data between functional units without the need to store them in \zkratkatext{RF}s (\zk{RF}).
\zk{FU}s can distribute data between each other and thus reducing the pressure put on \zk{RF}s.
We can have \zk{RF}s with a lower number of read and write ports and it is possible to implement them as traditional functional units \citep{corporal98}.

Some \zk{FU}s have an access to a data memory.
These are called \zk{LSU}s (\zkratkatext{LSU}s).
The program instructions are stored in an \zk{IM} (\zkratkatext{IM}) and executed by a \zk{GCU} (\zkratkatext{GCU}) which decodes them into the data transports.

An example architecture can be found in Fig. \ref{fig:tta_example}.
The \zkratkatext{IC} consists of 6 buses.
To these buses inputs and outputs of \zk{FU}s, \zk{GCU}, \zk{RF}s and \zk{LSU} are connected in a desired way.
Connections to the buses are marked by a dot.
Small asterlisks near the inputs of \zk{FU}s mark the trigger input.
By writing data to this port the desired operation will be triggered.
Each \zk{FU} can have multiple operations which are distinguished by an opcode provided with a data in the trigger port (the opcode is generated automatically by the compiler).
These operations can be generic (ADD, MUL, SHIFT, etc.) or we have the possibility to design our own and connect them via a standardized interface.

\begin{figure}[htb]
	\centering
	\resizebox{1\textwidth}{!}{\import{obrazky/}{tta_example.pdf_tex}}
	\caption{Example of a TTA processor configuration}
	\label{fig:tta_example}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{TTA-Based Co-Design Environment}
\label{tta:tce}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\zkratkatext{TCE} (\zk{TCE}) \citep{tce10} is a \zk{TTA} design toolset for a complete processor design and simulation from the high-level languages (C/C++) down to \zk{RTL} (\zkratkatext{RTL}) description (with support of VHDL or Verilog).

The toolset consists of a group of programs more or less tied together, sometimes wrapped in a \zk{GUI} (\zkratkatext{GUI}).
Future paragraphs give a brief overview of the ones used in this project.
More info can be found in the toolset's documentation \citep{tce}.

\paragraph{ProDe}
Processor Designer is a \zk{GUI} program where the architecture is defined.
\zk{FU}s and their operations, registers and memory interface units are defined connected to the \zk{TTA} interconnection network.
The exact behaviour of the \zk{FU} does not have to be implemented in order to be able to set it in ProDe.
The only mandatory parameter is the operation's latency.
The units in ProDe are more like \uv{shells} for the operations.
The operations' behaviour can be added or changed later.
Prode can also produce implementation definition files which map RTL descriptions of \zk{FU}s to the \zk{FU}s placed within the software.
These files can later be used in ProGe.

\paragraph{tcecc (tceasm)}
Compiler for a software written in C/C++ (or parallel assembly) for a \zk{TTA} architecture.
The compiler is retargetable which means that together with the program source file (C/C++ or parallel assembly) it is necessary to specify also the target architecture.

\paragraph{Proxim (ttasim)}
Proxim is a GUI version of ttasim - a processor simulator.
It loads an architecture from ProDe, program from tcecc and simulates the program running on the architecture.
It is possible to get various statistics such as a \zk{FU}s' usage or a cycle count.
The program also allows stepping through the program by one clock cycle, observing the instructions' execution and contents of a data memory, registers and \zk{FU}s' ports.
For the simulation it is necessary that operations of \zk{FU}s have a defined behaviour in C/C++.

\paragraph{OSEd}
This program is a library and a manager of operations defined in C/C++ for the purpose of a simulation.
It is possible to assign source files to particular operations and compile them.
Also various operations' properties are set there (number of inputs/outputs, description, etc.).
OSEd also contains a tester where it is possible to test the operation before it is used in a program.
Many basic operations (add, substract, bit operaitons, memory read/write, etc.) are already included.
It is possible to make a fully functional processor only with operations provided by \zk{TCE}.

\paragraph{HDBEditor}
HDBEditor is responsible for mapping \zk{FU}s to their \zk{RTL} descriptions.
This is not needed for the simulation purposes but it is necessary if we want to generate the \zk{RTL} description of the processor.
The tool creates a hardware database where a user groups the desired descriptions together.
It is possible to have several variants of one \zk{FU} and choose which one to use upon the processor generation.
All the operations included in \zk{TCE} by default are also implemented in a \zk{HDL} and added to the default databases.
There is, however,  a difference between OSEd and HDBEditor in terms of what they describe.
While OSEd describes operations separately, HDBEditor describes whole \zk{FU}s.
This means that for example an \zk{ALU} (\zkratkatext{ALU}) functional unit with operations \uv{add} and \uv{shl} and an \zk{ALU} with operations \uv{add}, \uv{shl} and \uv{sub} need to have two different RTL implementations even though they share some operations.
However, the \zk{FU}s' interface is standardized and thus creating new \zk{FU}s from existing operations is a matter of copy-pasting.

\paragraph{ProGe}
Processor Generator produces a synthesizeable RTL description of the whole processor.
Its inputs are the processor architecture file and the implementation definition file.
There is also an option to generate testbenches for the processor verification.
ProGe also includes a platform integrator which can be used to interface the processor with specific \zk{FPGA} \zk{FPGA} boards.

\paragraph{PIG}
Program Image Generator converts a compiled program (from tcecc or tceasm) into a binary image.
This can be uploaded into a target machine's memory and executed as a program on an implemented machine.
It is possible to apply a compression to reduce an instruction memory size.
