\chapter{FFT Algorithms}
\label{fft}

\zkratkatext{DFT} (\zk{DFT}) is arguably one of the most frequently used algorithms in digital signal processing. It converts a finite sequence of equally spaced samples of a signal into a sequence of equally spaced coefficients representing amplitudes of complex sinusoids. We can write this relation as follows \cite{schafer}:
\begin{align}
	X(k) &= \frac{1}{N} \sum_{k=0}^{N-1} x(n) W_N^{kn} \ ,
\end{align}
where $W_N$ denotes a complex sinusoid:
\begin{align}
	W_N &= e^{-j 2 \pi / N} = \cos{(2 \pi / N)} - j \sin{(2 \pi / N)} \ .
\end{align}
$X(k)$ is the Fourier series and $x(n)$ is the input sampled signal. both $k$ and $n$ are integers in the interval $[0,N-1]$ where $N$ is the size of the Fourier transform. $j$ is the imaginary unit.

A direct calculation would require $N^2$ operations (complex multiplication followed by a complex addition). It is possible to reduce the computation complexity by decomposing the \zk{DFT} into a series of smaller \zk{DFT}s \cite{cooley}. Sections \ref{fft:radix} - \ref{fft:mixed_radix} are dedicated to this problem.

By examining the coefficients $W_N = e^{-j 2 \pi / N}$ it is possible to observe certain periodicities and symmetries which can be exploited to reduce both a memory usage and a computation complexity. This topic is further described in Sections \ref{fft:twiddles} - \ref{fft:memory}.

\section{Radix-$p$ Algorithms}
\label{fft:radix}
According to Cooley-Turkey principle if we can divide $N = P Q$, then $N$-point \zk{DFT} can be computed using $P$-point and $Q$-point \zk{DFT}s. If we can further decompose the \zk{DFT} in a way that $N = p^q$, we talk about radix-$p$ \zk{FFT} algorithm. In the future work we will focus only on radix-2 and radix-4 algorithms.

In general there are two approaches in computing the \zk{FFT}. First one, \zkratkatext{DIT} (\zk{DIT}) is based on successively decomposing the input sequence $x(n)$ in smaller subsequences. Inversely, \zkratkatext{DIF} (\zk{DIF}) algorithm is based on dividing the output sequence $X(k)$ \cite{schafer}. Since both approaches are equivalent in terms of an arithmetic complexity \cite{pitkanen} we will further focus only on the \zk{DIT} approach.

\section{Radix-2}
\label{fft:radix2}
If $N = 2^q$, then we call the algorithm a radix-2 \zk{FFT} algorithm. We can divide the input sequence $x(n)$ into two subsequences of even and odd numbered $n$-s. By substituing $n = 2r$ for $n$ even and $n = 2r+1$ for $n$ odd we obtain \cite{schafer}
\begin{align}
\begin{split}
	X(k) &= \sum_{r=0}^{N/2-1} x(2r) W_N^{2rk} + \sum_{r=0}^{N/2-1} x(2r+1) W_N^{(2r+1)k} \\
	     &= \sum_{r=0}^{N/2-1} x(2r) (W_N^2)^{rk} + W_N^k \sum_{r=0}^{N/2-1} x(2r+1) (W_N^2)^{rk} \ .
\end{split}
\end{align}
With the knowledge of $W_N^2 = W_{N/2}$ we can rewrite the previous equation in this form (substituing $n$ by $2r$ or $2r+1$):
\begin{align}
\begin{split}
	X(k) &= \sum_{r=0}^{N/2-1} x(2r) W_{N/2}^{rk} + W_N^k \sum_{r=0}^{N/2-1} x(2r+1) W_{N/2}^{rk} \\
	     &= G(k) + W_N^k H(k) \ .
\end{split}
\end{align}

The coefficients $W_{N}^{k}$ are called \textit{twiddle factors}. In the former equation $G(k)$ and $H(k)$ are two $N/2$-point \zk{DFT}s. Since $N = 2^q$ we can apply the same principle by dividing each of them into two parts again. Thus we would obtain 4 $N/4$-point \zk{DFT}s. By repeating $q$-times we can proceed until the computation is reduced to only 2-point DFTs. This elementary 2-point DFT is called a \textit{radix-2 butterfly} and its dataflow is illustrated in Fig. \ref{fig:rdx2_but}. Arrows denote a multiplication (when no operand is present, 1 is assumed - therefore just a straight transmission), additions are denoted by a plus sign in a circle. Solid black dost denote a signal junction.

\begin{figure}[htb]
	\centering
	\resizebox{0.5\textwidth}{!}{\import{obrazky/}{rdx2_but.pdf_tex}}
	\caption{Radix-2 butterfly}
	\label{fig:rdx2_but}
\end{figure}

For the radix-2 butterfly it is possible to write
\begin{align}
\label{eq:rdx2_but}
\begin{split}
	y_0 &= x_0 + W_N^r x_1 \\
	y_1 &= x_1 - W_N^r x_1 \ .
\end{split}
\end{align}
The resulting arithmetic complexity for one butterfly is two complex additions and one complex multiplication. Each stage contains $N/2$ butterflies and the whole algorithm has $\log _2 N$ stages. For the whole $N$-point \zk{FFT} $N \log _2 N$ complex additions and $N/2 \log _2 N$ complex multiplications are needed.

\section{Radix-4}
\label{fft:radix4}

In case of $N = 4^q$ the \zk{DFT} algorithm is called radix-4 \zk{FFT}. Radix-2 algorithm divides the input sequence $x(n)$ into two subsequences according to $n$ being even or odd ($\bmod{2}$). Radix-4 \zk{FFT} divides the sequence into 4 $N/4$ \zk{DFT}s based on $n \bmod{4}$. In the next stage we break each of the $N/4$ stage into 4 parts again and continue until the computation is reduced to an elementary 4-point \zk{DFT} - \textit{radix-4 butterfly}. The resulting sequence can be written as follows (with $n$ substitued by $4r$, $4r+1$, $4r+2$ or $4r+3$ and $W_N^4 = W_{N/4}$):

\begin{align}
\begin{split}
	X(k) &= \frac{1}{N} \sum_{k=0}^{N-1} x(n) W_N^{kn}  \\
	%	 &= \sum_{r=0}^{N/4-1} x(4r) W_N^{4rk}
	%	  + \sum_{r=0}^{N/4-1} x(4r+1) W_N^{(4r+1)k}     \\
	%     &+ \sum_{r=0}^{N/4-1} x(4r+2) W_N^{(4r+2)k}
	%	  + \sum_{r=0}^{N/4-1} x(4r+3) W_N^{(4r+3)k}
	     &=          \sum_{r=0}^{N/4-1} x(4r)   W_{N/4}^{rk}
		  + W_N^k    \sum_{r=0}^{N/4-1} x(4r+1) W_{N/4}^{rk} \\
		 &+ W_N^{2k} \sum_{r=0}^{N/4-1} x(4r+2) W_{N/4}^{rk}
		  + W_N^{3k} \sum_{r=0}^{N/4-1} x(4r+3) W_{N/4}^{rk}
\end{split}
\end{align}

The radix-4 butterfly structure is shown in Fig. \ref{fig:butterflies} and its output can be written according to the Equation \ref{eq:rdx4_but}.

\begin{figure}[htb]
	\centering
	\resizebox{1\textwidth}{!}{\import{obrazky/}{butterflies.pdf_tex}}
	\caption{Radix-4 butterfly can be computed combining several Radix-2 butterflies}
	\label{fig:butterflies}
\end{figure}

\begin{align}
\label{eq:rdx4_but}
\begin{split}
	y_0 &= x_0 +  W_N^{k_1} x_1 + W_N^{k_2} x_2 +  W_N^{k_3} x_3 \\
	y_1 &= x_0 - jW_N^{k_1} x_1 - W_N^{k_2} x_2 + jW_N^{k_3} x_3 \\
	y_2 &= x_0 -  W_N^{k_1} x_1 + W_N^{k_2} x_2 -  W_N^{k_3} x_3 \\
	y_3 &= x_0 + jW_N^{k_1} x_1 - W_N^{k_2} x_2 - jW_N^{k_3} x_3
\end{split}
\end{align}

One radix-4 butterfly has an arithmetic complexity of 12 complex additions and 3 complex multiplications. Each stage contains $N/4$ butterflies and the whole radix-4 \zk{FFT} can be performed in $\log _4 N$ stages. Therefore the total arithmetic cost is $3 N \log _4 N$ complex additions and $\frac{3N}{4} \log _4 N$ complex multiplications which is less than in radix-2 algorithm. Apart from that the number of stages is halved compared to radix-2.

Higher-than-four radices (i.e. 8) are also possible to reduce the arithmetic complexity but they require more complex butterfly coefficients than simple $\pm 1$ and $\pm j$.

The main limitation of radix-4 (and any higher) \zk{FFT} is that it requires $N$ to be a power of four which greatly reduces the possible \zk{FFT} sizes. This limitation can be overcome by using a \textit{mixed radix} approach described in Section \ref{fft:mixed_radix}.

\section{Mixed Radix}
\label{fft:mixed_radix}

Mixed radix \zk{FFT} is intended in cases where we want the advantages of radix-4 butterflies but need a support all \zk{FFT} sizes $N = 2^q$. The mixed radix \zk{FFT} computes all stages except the last using radix-4 butterflies. If $q$ is an odd number the last stage is computed with radix-2 butterflies, otherwise radix-4 is used even for the last stage. In this work the mixed radix approach is used.

\section{Twiddle Factors}
\label{fft:twiddles}

A special attention needs to be paid to the \zk{FFT} coefficients - twiddle factors. They have the following form:
\begin{align}
	W_N^k &= e^{-j 2 \pi k/N} = \cos{(2 \pi k/N)} - j \sin{(2 \pi k/N)} \ .
\end{align}
It can be observed that they are points on a complex unity circle, as shown in Figure \ref{fig:twiddles}. The figure contains all twiddle factors needed for computing \zk{FFT} of size 64 (radix-4). Twiddle factors of sizes 32 (mixed radix) and 16 (radix-4) are shown using different markers and it is possible to obtain them from the 64-\zk{FFT} coefficients.

In \cite{pitkanen} different methods are compared. Generally speaking we want to avoid direct computation which can create a big computation overhead and slow down the computation. Instead a method bases on a \zkratkatext{LUT} (\zk{LUT}) is used. All the necessary twiddle factors are pre-computed and stored in a memory for a future access. This method is the fastest but requires more memory and area. However, it is not necessary to store all the twiddle factors. Possible memory optimization is described in Section \ref{fft:memory}.

\begin{figure}[htb]
	\centering
	\resizebox{1\textwidth}{!}{\import{obrazky/}{twiddles.pdf_tex}}
	\caption{Twiddle factors of FFT size 64}
	\label{fig:twiddles}
\end{figure}

\section{Memory optimization}
\label{fft:memory}

According to Sections \ref{fft:radix2} and \ref{fft:radix4} the total number of radix-2 (radix-4) butterflies is $N/2 \log _2 N$ ($N/4 \log _4 N$). Each butterfly contains 1 (3) multiplications with a non-trivial (not equal to one) twiddle factor. If we assume $N=64$, the needed amount of memory entries in the \zk{LUT} would need to be 192 (144).

However, if we allow some additional logic before the \zk{LUT} it is possible to reduce the number of memory entries to the first $N/8+1$ \cite{pitkanen} coefficients from the first octant (sector B0 in the Figure \ref{fig:twiddles}). \zk{FFT} of a size 64 therefore needs only the first 9 values. Other values can be computed from the first octant by simple operations such as inverting real and imaginary parts and multiplying by $-1$. In Figure \ref{fig:twiddles} the relation needed to obtain the value in each sector is shown in the sector. Asterlisk sign $*$ denotes a complex conjugate operation.
